package top.mtain.dm;

import java.sql.*;

public class DMJDBC {

    public static void main(String[] args) {
        Connection con;
        String driver = "dm.jdbc.driver.DmDriver";
        String url = "jdbc:dm://127.0.0.1:5236/DEMO";
        String user = "DEMO";
        String password = "DEMODEMODEMO";
        try {
            Class.forName(driver);
            con = DriverManager.getConnection(url,user,password);
            if(!con.isClosed())
                System.out.println("Succeeded connecting to the Database!");

            Statement statement = con.createStatement();
            //要执行的SQL语句
            String sql = "select * from Persons";
            //3.ResultSet类，用来存放获取的结果集！！
            ResultSet rs = statement.executeQuery(sql);
            System.out.println("-----------------");
            System.out.println("执行结果如下所示:");
            System.out.println("-----------------");
            System.out.println("编号" + "\t" + "姓名");
            System.out.println("-----------------");

            String id = null;
            String name = null;
            while(rs.next()){
                id = rs.getString("ID");
                name = rs.getString("Name");

                System.out.println(id + "\t" + name);
            }
            rs.close();
            con.close();
        } catch(ClassNotFoundException e) {
            System.out.println("Sorry,can`t find the Driver!");
            e.printStackTrace();
        } catch(SQLException e) {
            //数据库连接失败异常处理
            e.printStackTrace();
        }catch (Exception e) {
            e.printStackTrace();
        }finally{
            System.out.println("数据库数据成功获取！！");
        }
    }
}
