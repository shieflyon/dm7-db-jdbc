## 达梦数据库-JDBC示例

## 创建数据库
### 使用达梦数据库自带的"DM管理工具"创建数据库
1. 使用达梦管理员用户`SYSDBA`登录,用户->管理用户->右键新建用户

   ![20190423150739](http://www.mtain.top/git-pic/20190423150739.png)

2. 输入用户名称和大于9位的密码

   ![20190423150803](http://www.mtain.top/git-pic/20190423150803.png)

3. 设置权限,点击确定即可

   ![20190423150823](http://www.mtain.top/git-pic/20190423150823.png)

4. 创建用户会同时创建同名数据库(类似于Oracle)

   ![20190423151024](http://www.mtain.top/git-pic/20190423151024.png)



## 使用命令行建表并插入数据

1. 连接本地数据库

```shell
[root@dm tool]# ./disql
disql V7.6.0.142-Build(2019.03.12-103811)ENT
SQL>  conn DEMO/DEMODEMODEMO

```

2. 执行建表和插入数据的SQL语句

```shell
CREATE TABLE Persons(ID int NOT NULL,Name varchar(255) NOT NULL);

insert into Persons values(1,'赵一');
insert into Persons values(2,'钱二');
insert into Persons values(3,'孙三');
insert into Persons values(4,'李四');
insert into Persons values(5,'王五');
insert into Persons values(6,'刘六');
insert into Persons values(7,'宋七');
insert into Persons values(8,'周八');
commit
```





## 使用第三方工具连接数据库

**工具:** dbvisualizer

先自定义添加达梦的JDBC驱动,再创建数据库链接

![20190423153022](http://www.mtain.top/git-pic/20190423153022.png)



## 达梦数据库驱动
1. Maven依赖
    公网Maven仓库中没有达梦的依赖,需要自行安装到私库或者本地仓库
    
    本地仓库安装命令:

    `mvn install:install-file -DgroupId=com.dm -DartifactId=dmjdbc7 -Dversion=1.7.0 -Dpackaging=jar -Dfile=Dm7JdbcDriver17.jar`

    Maven POM
    ```
    <!-- jdbc驱动包 -->
    <dependency>
        <groupId>com.dm</groupId>
        <artifactId>dmjdbc7</artifactId>
        <version>1.7.0</version>
    </dependency>
    <!-- hibernate方言包 -->
    <dependency>
        <groupId>com.dm.dialect</groupId>
        <artifactId>hibernate4</artifactId>
        <version>4.0</version>
    </dependency>
    
    ```

2. JAR文件

    项目lib目录下